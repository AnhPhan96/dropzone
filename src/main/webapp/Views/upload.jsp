<%--
  Created by IntelliJ IDEA.
  User: Anh
  Date: 10/30/2017
  Time: 4:41 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" name="viewport"
          content="width=device-width, initial-scale=1">
    <title>Dropzone Example</title>
    <link rel="stylesheet" type="text/css"
          href='<c:url value="/css/style.css"/>'>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript"
            src='<c:url value="/js/dropzone.js"/>'></script>
    <script type="text/javascript"
            src='<c:url value="/js/app.js"/>'></script>
</head>
<body>
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading text-center">
            <h3>Dropzone Example</h3>
        </div>
        <div class="panel-body">
            <div>
                <form id="dropzone-form" class="dropzone" action="uploadDropzone"
                      enctype="multipart/form-data">

                    <div class="dz-default dz-message file-dropzone text-center well col-sm-12">
                        <span class="glyphicon glyphicon-paperclip"></span> <span>
								To attach files, drag and drop here</span><br> <span>OR</span><br>
                        <span>Just Click</span>
                    </div>


                    <div class="dropzone-previews"></div>
                </form>
                <hr>
                <button id="upload-button" class="btn btn-primary">
                    <span class="glyphicon glyphicon-upload"></span> Upload
                </button>
                <a class="btn btn-primary pull-right" href="listImage">
                    <span class="glyphicon glyphicon-eye-open"></span> View All Uploads
                </a>
            </div>
        </div>
    </div>
</div>
</body>
</html>
