package com.example.anh.dropzone.controller;

import com.example.anh.dropzone.entity.Image;
import com.example.anh.dropzone.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

@Controller
public class ImageController {

    @Autowired
    ImageService imageService;

    private String UPLOAD_PATH = "C:\\Users\\Anh\\Downloads\\Compressed\\Example201719\\dropzone\\src\\main\\resources\\image\\";

    @GetMapping("/chooseImage")
    public String chooseImage(){
        return "upload";
    }

    @PostMapping("/uploadDropzone")
    public void uploadDropzone(MultipartHttpServletRequest request,
                               HttpServletResponse response) throws IOException {
        Map<String, MultipartFile> fileMap = request.getFileMap();

        for (MultipartFile file : fileMap.values()) {
            byte[] imageData = file.getBytes();
            Path path = Paths.get(UPLOAD_PATH + file.getOriginalFilename());
            Files.write(path, imageData);

            String imagePath = "/uploadFile/" + file.getOriginalFilename();
            imageService.saveImage(imagePath);
        }
    }

    @GetMapping("/listImage")
    public ModelAndView listImage(ModelAndView model){
        List<Image> images = imageService.getImages();
        model.addObject("images",images);
        model.setViewName("listImage");
        return model;
    }

    @ExceptionHandler(IOException.class)
    public ResponseEntity<?> handleIOException(HttpServletRequest request, HttpServletResponse response) {
        response.setStatus(HttpStatus.BAD_REQUEST.value());
        return ResponseEntity.badRequest().build();
    }
}
