package com.example.anh.dropzone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DropzoneApplication {

	public static void main(String[] args) {
		SpringApplication.run(DropzoneApplication.class, args);
	}
}
