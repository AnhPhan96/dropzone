package com.example.anh.dropzone.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class BeanConfig extends WebMvcConfigurerAdapter {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry
                .addResourceHandler("/uploadFile/**")
                .addResourceLocations("file:C:\\Users\\Anh\\Downloads\\Compressed\\Example201719\\dropzone\\src\\main\\resources\\image\\");
    }
}
