package com.example.anh.dropzone.repository;

import com.example.anh.dropzone.entity.Image;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageRepository extends JpaRepository<Image, Integer> {

}
