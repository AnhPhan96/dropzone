package com.example.anh.dropzone.service;

import com.example.anh.dropzone.entity.Image;

import java.util.List;

public interface ImageService {
    public void saveImage(String imagePath);
    public List<Image> getImages();
}
