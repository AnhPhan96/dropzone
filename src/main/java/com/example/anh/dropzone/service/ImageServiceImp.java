package com.example.anh.dropzone.service;

import com.example.anh.dropzone.entity.Image;
import com.example.anh.dropzone.repository.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImageServiceImp implements ImageService {

    @Autowired
    private ImageRepository imageRepository;

    @Override
    public void saveImage(String imagePath) {
        Image image = new Image(imagePath);
        imageRepository.save(image);
    }

    @Override
    public List<Image> getImages() {
        return imageRepository.findAll();
    }
}
